﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace QuickBaseEmailBridge
{
    public partial class Service1 : ServiceBase
    {
        public EventLog qbLog = new EventLog();
        public const String qbTableIDSettings = "bmsy8qtmm";

        public Service1()
        {
            InitializeComponent();
            if (!System.Diagnostics.EventLog.SourceExists("QuickbaseSource"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "QuickbaseSource", "QuickbaseLog");
            }
            
            qbLog.Source = "QuickbaseSource";
            qbLog.Log = "QuickbaseLog";
        }

        protected override void OnStart(string[] args)
        {
            qbLog.WriteEntry("Started Quickbase Email Bridge.");

            //now setup timer interval
            // Set up a timer to trigger every minute.  
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 60000; // 60 seconds  
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            // TODO: Insert monitoring activities here.  
            qbLog.WriteEntry("Quickbase Email Bridge check", EventLogEntryType.Information);

            //get settings from quickbase
            qbLog.WriteEntry(getSettings());
            //check for mail
            //insert any tasks if they exist
        }

        protected override void OnStop()
        {
            qbLog.WriteEntry("Stopped Quickbase Email Bridge.");
        }

        protected override void OnContinue()
        {
            qbLog.WriteEntry("Quickbase Email Bridge running");
        }

        private String getSettings()
        {
            //this function will get the configuration from quickbase for the email connector
            String result = QuickbaseApiWrapper.Authenticate("toaddixon@yahoo.com", "   ");
            if (result.StartsWith("Quickbase login failed: "))
            {//log that it failed
                qbLog.WriteEntry(result);
            } else
            {
                //fetch the settings with the saved token
                DataTable dt_settings = QuickbaseApiWrapper.QryTable(result, qbTableIDSettings, "6.7"); //6=tag 7=value
                qbLog.WriteEntry("fetched=" + dt_settings.Rows.Count);
                String tempOutputString = "";
                foreach (DataRow row in dt_settings.Rows)
                {
                    tempOutputString = tempOutputString + "tag=" + row["Tag"].ToString() + " value=" + row["Value"].ToString() + Environment.NewLine;
                }
                qbLog.WriteEntry(tempOutputString);
            }

            return result;
            
        }


        //quickbase interface
        public static class Apis
        {
            public const string Authenticate = "API_Authenticate";
            public const string DoQuery = "API_DoQuery";
            public const string GetDbInfo = "API_GetDBInfo";
            public const string GenResultsTable = "API_GenResultsTable";
            public const string API_EditRecord = "API_EditRecord";
            public const string API_FindDBByName = "API_FindDBByName";
            public const string API_DoQueryCount = "API_DoQueryCount";
            public const string API_AddRecord = "API_AddRecord";
            public const string API_DeleteRecord = "API_DeleteRecord";
            public const string API_CreateTable = "API_CreateTable";
            public const string API_AddField = "API_AddField";


        }
        public static class QuickbaseApiWrapper
        {
            //const string dbId1 = "bmffvki3i";
            //const string queryId = "1";
            //const string tableID = qbRatesTableID;
            const string applicationToken = "d8exemsb5pdwmzznpbm6enu92d"; //dev
            const string siteURL = "https://tklaw.quickbase.com/";

            //refreshed
            public static string Authenticate(string username, string password)
            {
                StringDictionary parameters = new StringDictionary();
                parameters.Add("username", username);
                parameters.Add("password", password);
                string url = siteURL + "db/main?a=" + Apis.Authenticate;

                foreach (string s in parameters.Keys)
                {
                    url += "&" + s + "=" + parameters[s];
                }

                DataSet ds = GetQueryResults(url, Apis.Authenticate, parameters);

                string errorMessage = ds.Tables[0].Rows[0]["errtext"].ToString();
                string errorCode = ds.Tables[0].Rows[0]["errcode"].ToString();
                if (errorCode != "0")
                {
                    //need to figure out how to throw back error to event log here.
                    return "Quickbase login failed: " + errorMessage + ", code:" + errorCode;
                }
                else
                    return ds.Tables[0].Rows[0]["ticket"].ToString();
            }


            //not looked at in depth
            private static string CreateRootDoc(string apiName, StringDictionary parameters)
            {
                XmlDocument doc = new XmlDocument();
                XmlElement root = doc.CreateElement("qdbapi");

                if (parameters != null)
                {
                    foreach (string field in parameters.Keys)
                    {
                        XmlElement child = doc.CreateElement(field);
                        child.InnerText = parameters[field];
                        root.AppendChild(child);
                    }
                }
                doc.AppendChild(root);
                StringWriter sw = new StringWriter();
                XmlTextWriter xw = new XmlTextWriter(sw);
                doc.WriteTo(xw);

                return sw.ToString();
            }


            private static System.Data.DataSet GetQueryResults(string url, string action, StringDictionary parameters)
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.Method = "POST";
                webRequest.ContentType = "application/xml";
                webRequest.Headers.Add("QUICKBASE-ACTION", action);
                string input = CreateRootDoc(action, parameters);
                byte[] postBytes = Encoding.ASCII.GetBytes(input);
                webRequest.ContentLength = postBytes.Length;
                Stream postStream = webRequest.GetRequestStream();
                postStream.Write(postBytes, 0, postBytes.Length);
                postStream.Close();
                WebResponse response = webRequest.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());

                XmlTextReader xmlReader = new XmlTextReader(reader);
                StringDictionary toGo = new StringDictionary();
                System.Data.DataSet ds = new System.Data.DataSet();
                ds.ReadXml(xmlReader, System.Data.XmlReadMode.Auto);

                return ds;
            }

            private static DataTable GenResultsTable(string dbIdParam, string action, StringDictionary parameters)
            {
                string url = siteURL;
                if (dbIdParam != null)
                    url += "db/" + dbIdParam;
                url += "?act=" + action;
                foreach (string s in parameters.Keys)
                {
                    url += "&" + s + "=" + parameters[s];
                }

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);

                webRequest.Method = "POST";
                webRequest.ContentType = "application/xml";
                webRequest.Headers.Add("QUICKBASE-ACTION", action);
                string input = CreateRootDoc(action, parameters);
                byte[] postBytes = Encoding.ASCII.GetBytes(input);
                webRequest.ContentLength = postBytes.Length;
                Stream postStream = webRequest.GetRequestStream();
                postStream.Write(postBytes, 0, postBytes.Length);
                postStream.Close();
                WebResponse response = webRequest.GetResponse();


                Console.Error.WriteLine(response.GetResponseStream());

                StreamReader reader = new StreamReader(response.GetResponseStream());
                //Console.WriteLine("Response::::" + reader.ReadToEnd());

                XmlTextReader xmlReader = new XmlTextReader(reader);
                XmlDocument docs = new XmlDocument();
                docs.Load(xmlReader);
                XmlElement root = docs.DocumentElement;
                StringDictionary toGo = new StringDictionary();
                DataTable dt = new DataTable();
                //get the columns created
                XmlNodeList nodes = root.SelectNodes("//qdbapi/table/fields/field");
                foreach (XmlNode node in nodes)
                {
                    Console.WriteLine("Field=" + node["label"].InnerText.ToString());
                    dt.Columns.Add(node["label"].InnerText.ToString());
                }

                //then populate with values
                nodes = root.SelectNodes("//qdbapi/table/records/record");
                String[] rowarray = new String[dt.Columns.Count];
                foreach (XmlNode node in nodes)
                {
                    //Console.WriteLine("Value=" + node["f"].InnerText.ToString());
                    int i = 0;
                    foreach (XmlNode childnode in node.ChildNodes)
                    {

                        String value;
                        if (childnode.InnerXml.ToString() != null && childnode.Name == "f")
                        {
                            value = childnode.InnerXml.ToString();
                            //Console.WriteLine("Value=" + value);
                            rowarray[i] = value;
                            i++;
                        }
                        else if (childnode.Name == "f")
                        {
                            value = "";
                            //Console.WriteLine("Value=" + value);
                            rowarray[i] = value;
                            i++;
                        }

                    }
                    dt.Rows.Add(rowarray);
                }

                dt.ReadXml(xmlReader);

                return dt;
            }

            

            public static DataTable QryTable(string ticket, string qbID, string cList)
            {
                StringDictionary parameters = new StringDictionary();
                parameters.Add("dbid", qbID);
                parameters.Add("apptoken", applicationToken);
                parameters.Add("ticket", ticket);
                parameters.Add("clist", cList);
                parameters.Add("fmt", "structured");
                return GenResultsTable(qbID, Apis.DoQuery, parameters);
            }

            public static DataTable QryTableQuery(string ticket, string qbID, string cList, string query)
            {
                StringDictionary parameters = new StringDictionary();
                parameters.Add("dbid", qbID);
                parameters.Add("apptoken", applicationToken);
                parameters.Add("ticket", ticket);
                parameters.Add("clist", cList);
                parameters.Add("fmt", "structured");
                parameters.Add("query", query);
                return GenResultsTable(qbID, Apis.DoQuery, parameters);
            }

            public static int InsertRecords(string ticket, string dbIdParam, DataTable insertRows)
            {

                StringDictionary parameters = new StringDictionary();
                parameters.Add("dbid", dbIdParam);
                parameters.Add("apptoken", applicationToken);
                parameters.Add("ticket", ticket);

                //generate xml
                String xml = "<qdbapi><udata>mydata</udata><ticket>" + ticket + "</ticket>";

                foreach (DataRow row in insertRows.Rows)
                {
                    foreach (DataColumn col in insertRows.Columns)
                        xml += "<field name=\"" + col.ColumnName + "\">" + row[col].ToString() + "</field>";
                }
                //return respose
                xml += "</qdbapi>";
                Console.WriteLine("XML is ->" + xml);


                string url = siteURL;
                if (dbIdParam != null)
                    url += "db/" + dbIdParam;
                url += "?act=" + Apis.API_AddRecord;
                foreach (string s in parameters.Keys)
                {
                    url += "&" + s + "=" + parameters[s];
                }

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);

                webRequest.Method = "POST";
                webRequest.ContentType = "application/xml";
                webRequest.Headers.Add("QUICKBASE-ACTION", Apis.API_AddRecord);
                byte[] postBytes = Encoding.ASCII.GetBytes(xml);
                webRequest.ContentLength = postBytes.Length;
                Stream postStream = webRequest.GetRequestStream();
                postStream.Write(postBytes, 0, postBytes.Length);
                postStream.Close();
                WebResponse response = webRequest.GetResponse();


                Console.Error.WriteLine(response.GetResponseStream());

                StreamReader reader = new StreamReader(response.GetResponseStream());

                //get the return id of the inserted record
                String returnID = "";
                XmlTextReader xmlReader = new XmlTextReader(reader);
                XmlDocument docs = new XmlDocument();
                docs.Load(xmlReader);
                XmlElement root = docs.DocumentElement;
                XmlNodeList nodes = root.SelectNodes("//qdbapi");
                foreach (XmlNode node in nodes)
                {
                    foreach (XmlNode childnode in node.ChildNodes)
                    {
                        if (childnode.InnerXml.ToString() != null && childnode.Name == "errcode")
                        {
                            String errorCode = childnode.InnerXml.ToString();
                            if (!errorCode.Equals("0"))
                            {
                                //then we filp to negative number and return the error code instead of the RID
                                return (Int32.Parse(errorCode) * -1);
                            }
                        }

                        if (childnode.InnerXml.ToString() != null && childnode.Name == "rid")
                        {
                            returnID = childnode.InnerXml.ToString();
                            Console.WriteLine("ReturnValue=" + returnID);
                        }
                    }
                }

                return (Int32.Parse(returnID));
            }

            public static String UpdateRecord(string ticket, string dbIdParam, String recordID, DataTable updateFieldValues)
            {

                StringDictionary parameters = new StringDictionary();
                parameters.Add("dbid", dbIdParam);
                parameters.Add("apptoken", applicationToken);
                parameters.Add("ticket", ticket);

                //generate xml
                String xml = "<qdbapi><udata>mydata</udata><ticket>" + ticket + "</ticket><rid>" + recordID + "</rid>";

                foreach (DataRow row in updateFieldValues.Rows)
                {
                    foreach (DataColumn col in updateFieldValues.Columns)
                        xml += "<field name=\"" + col.ColumnName + "\">" + row[col].ToString() + "</field>";
                }
                //return respose
                xml += "</qdbapi>";
                Console.WriteLine("XML is ->" + xml);


                string url = siteURL;
                if (dbIdParam != null)
                    url += "db/" + dbIdParam;
                url += "?act=" + Apis.API_AddRecord;
                foreach (string s in parameters.Keys)
                {
                    url += "&" + s + "=" + parameters[s];
                }

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);

                webRequest.Method = "POST";
                webRequest.ContentType = "application/xml";
                webRequest.Headers.Add("QUICKBASE-ACTION", Apis.API_EditRecord);
                byte[] postBytes = Encoding.ASCII.GetBytes(xml);
                webRequest.ContentLength = postBytes.Length;
                Stream postStream = webRequest.GetRequestStream();
                postStream.Write(postBytes, 0, postBytes.Length);
                postStream.Close();
                WebResponse response = webRequest.GetResponse();

                Console.Error.WriteLine(response.GetResponseStream());

                StreamReader reader = new StreamReader(response.GetResponseStream());

                Console.WriteLine("RESPONSE TO UPDATE===" + reader.ReadToEnd().ToString());

                return "";
            }

        }
        /////////end quickbase wrapper

    }
}
